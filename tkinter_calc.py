import tkinter as tk
import tkinter.ttk as ttk
from functools import partial
from interpreter import Interpreter
import tkinter.font as font

window = tk.Tk()
window.title("THE BEST CALC EVER!")
window.minsize(width=250, height=250)
window.geometry(f"{window.winfo_screenwidth() // 4}x{window.winfo_screenheight() // 4}")

main_frame = ttk.Frame(window)

window.rowconfigure(0, weight=1)
window.columnconfigure(0, weight=1)
main_frame.grid(row=0, column=0, sticky="news")

calc_input = tk.StringVar()
input_entry = ttk.Entry(main_frame, textvariable=calc_input)
input_entry.grid(column=0, row=0, columnspan=4, sticky="news")

whitelist = '0123456789-+/* =^'

interpreter = Interpreter()

button_font = font.Font(size=32)

def button_pressed(button_txt, *args):
    if button_txt != 'C' and calc_input.get() == "Error":
        button_pressed('C')

    banned_chars = ''.join(filter(lambda x: x not in whitelist, button_txt))
    if button_txt == '=':
        message = "Error"
        if banned_chars != '':
            message = "Banned chars in input"

        try:
            result = interpreter.interpret_line(calc_input.get())
            calc_input.set(f"{result:g}")
        except:
            pass
    elif button_txt == 'C':
        calc_input.set("")
    else:
        calc_input.set(calc_input.get() + button_txt)
    # print(banned_chars)


btn = ["123+", "456-", "789*", "C0=/"]
for i in range(1, 5):
    for j in range(4):
        btn_label = btn[i - 1][j]
        bp = partial(button_pressed, button_txt=btn_label)
        b = ttk.Button(main_frame, text=btn_label, command=bp)
        b.grid(column=j, row=i, sticky="news",)

main_frame.columnconfigure(tuple(range(4)), weight=1)
main_frame.rowconfigure(tuple(range(5)), weight=1)


window.mainloop()
